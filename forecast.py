import requests
import calendar

api_key = '0e117e2199473cc7a60ef3756dc4d5bc'
api_call = 'https://api.openweathermap.org/data/2.5/forecast?appid=' + api_key

running = True

print('5-Day Weather Forecast')

while running:
    city = input('Please input the city name: ')

    json_data = requests.get(api_call + '&q=' + city).json()

    location_data = {
        'city': json_data['city']['name'],
        'country': json_data['city']['country']
    }

    print('\n{city}, {country}'.format(**location_data))

    current_date = ''

    for item in json_data['list']:
        time = item['dt_txt']
        next_date, hour = time.split(' ')

        if current_date != next_date:
            current_date = next_date
            year, month, day = current_date.split('-')
            date = {'y': year, 'm': month, 'd': day}

            print('\n-----------------------------------------------------------')
            print('\n{m}/{d}/{y}'.format(**date))

        hour = int(hour[:2])

        if hour < 12:
            if hour == 0:
                hour = 12

            meridiem = 'AM'

        else:
            if hour > 12:
                hour -= 12

            meridiem = 'PM'

        print('\n%i:00 %s' % (hour, meridiem))

        temperature = item['main']['temp']
        humidity = item['main']['humidity']
        description = item['weather'][0]['description']
        windSpeed = item['wind']['speed']
        windDirection = item['wind']['deg']

        compassDirection = int((int(windDirection)/22.5) + .5)
        directionArr = ["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW",
            "SW","WSW","W","WNW","NW","NNW"]

        print('Weather condition: %s' % description)
        print('Humidity: ' + str(humidity))
        print('Temperature: {:.2f}'.format(temperature - 273.15) + " degrees celsius")
        print('Wind speed: ' + str(windSpeed) + 'm/s')
        print('Wind direction: ' + directionArr[(compassDirection % 16)] +
            ' (' + str(windDirection) + ' deg' + ')')


    while True:
        running = input('\n\nDo you want to search for forecasts of other cities? ')

        if running.lower() == 'yes' or running.lower() == 'y':
            print('Great!')
            break

        elif running.lower() == 'no' or running.lower() == 'n' or running == 'exit':
            running = False
            break

        else:
            print('Sorry, I didn\'t get that.')
